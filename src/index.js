/** ***************************************************************
 Copyright (C) 2023 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
import util from 'util';
import path from 'path';
import { exec as execProcess } from 'child_process';
import fs from 'fs';
import process from 'process';
import yaml from 'yaml';

const exec = util.promisify(execProcess);
// matches web URLs
const URL_REGEX = new RegExp('^https?://(?:[^./]+.){1,3}.[a-z]{2,3}(/[^/]+)*.[a-z]{3,4}?$');
// location where to save temp files for the run
const TEMP_DIR_NAME = 'tmp';
// the const to hold the generated spec data to be output for Hugo builds
const artificialSpecData = { items: [] };

/**
 * Pull an external/remote spec using fetch and write it to the file system for further processing.
 *
 * @param {*} externalLocation the URL for the raw openapi spec file.
 * @param {*} localPath the location where the downloaded spec should be saved
 */
async function pullExternalSpec(externalLocation, localPath) {
  let retries = 0;
  // retry 3 times before giving up
  while (retries < 3) {
    try {
      fs.writeFileSync(
        localPath,
        await fetch(externalLocation).then(response => {
          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }

          return response.text();
        })
      );
      return;
    } catch (e) {
      console.log('Error while fetching external spec', e);
    }
    retries++;
  }
  throw new Error(`Cannot process ${externalLocation} after 3 retries.`);
}

/**
 * Checks the passed string to see whether it is an external/remote spec.
 *
 * @param {*} location the potential URL denoting spec location
 * @returns true if the passed value is a remote URL, false otherwise
 */
function isExternalSpec(location) {
  return location != undefined && URL_REGEX.test(location);
}

/**
 * Process the spec using the redoc-cli build to compile the openapi spec into a human readable HTML spec.
 *
 * @param {string} specDisplayName the display name of the spec being processed
 * @param {string} specMachineName the machine name used for filepaths for the processed spec
 */
async function processSpec(specDisplayName, specMachineName) {
  console.log(`Processing openapi spec for ${specDisplayName}...`);
  // Go to the location where the spec is, and continue processing
  let retries = 0;
  // retry 3 times before giving up
  while (retries < 3) {
    try {
      await exec(
        `npx @redocly/cli build-docs --output="..${path.sep}content${path.sep}${specMachineName}${path.sep}index.html" "${specMachineName}${path.sep}openapi.yaml"`
      );
      console.log(`Done processing ${specDisplayName}`);
      return;
    } catch (e) {
      console.log('Error while processing spec', e);
    }
    retries++;
  }
  throw new Error(`Cannot process spec '${specDisplayName}' after 3 retries.`);
}

run();

/**
 * The actual build process, set into a function to gain access to async/await while building the various spec HTML files and the data 
 * file containing the pointers for the generated specs.
 */
async function run() {
  // Used for file presence checks
  const initialProjectDirectory = process.cwd();

  // Create the staging location if it doesn't yet exist
  fs.mkdirSync(TEMP_DIR_NAME, { recursive: true });
  // Go to temp dir to start work
  process.chdir(TEMP_DIR_NAME);

  // Create the data holder for the generated repo data definitions
  const reposDefinition = yaml.parse(fs.readFileSync('../repos.yaml', 'utf8'));
  for (const currentSpecDefinition of reposDefinition['specs']) {
    // get the variables from the yml definition of the spec
    const specMachineName = currentSpecDefinition['name'];
    const specDisplayName = currentSpecDefinition['displayName'] || currentSpecDefinition['name'];
    const specLocation = currentSpecDefinition['location'];
    const potentialLocalSpecLocation = `${initialProjectDirectory}${path.sep}${specLocation}`;
    const outputSpecLocation = `${specMachineName}${path.sep}openapi.yaml`;

    // Do prep for spec by creating folder in tmp dir to hold processed files
    fs.mkdirSync(specMachineName, { recursive: true });

    console.log(`Retrieving openapi spec for ${specDisplayName}...`);
    // prepare the spec, fetching remote specs or copying the spec for processing
    if (isExternalSpec(specLocation)) {
      await pullExternalSpec(specLocation, outputSpecLocation);
    } else {
      fs.copyFileSync(potentialLocalSpecLocation, outputSpecLocation);
      console.log(`Copied local spec for ${specDisplayName} to output dir`);
    }
    // process the spec, setting the information about the spec into the output data var
    await processSpec(specDisplayName, specMachineName).then(() => {
      artificialSpecData['items'].push({ name: specMachineName, displayName: specDisplayName });
    }).then(() => {
      console.log('Outputing generated spec data for site render');
      // Create the data dir if it doesn't exist
      fs.mkdirSync('../data', { recursive: true });
      // Dump the generated spec list to the data file for hugo build
      fs.writeFileSync('../data/specs.yaml', yaml.stringify(artificialSpecData));
    });
  };
}
