setup:;
	yarn install --frozen-lockfile
clean:;
	rm -rf tmp
build: clean setup;
run-local: build;
	hugo server