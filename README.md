# eclipsefdn-api-spec-host

## Compiling locally

To cold start this spec host to run locally, there is a make command available that will clean and fetch all required data and packages before running hugo server in line to start the server locally. This will bind hugo by default to port 1313 and make the site available at [http://localhost:1313/docs/api](http://localhost:1313/docs/api).

```
make run-local
```

## Adding new specs

To add new specs to the rendered site, a new line can be added in the `repos.properties` file. The key of the property is the display name to use in the URL and site generation, and the value of the property is the URL at which the spec can be found. Once added, re-running the make command above should allow for previews of the updated specification list.


## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [eclipsefdn-api-spec-host](https://gitlab.eclipse.org/eclipsefdn/it/webdev/eclipsefdn-api-spec-host) repository
2. Clone repository `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/eclipsefdn-api-spec-host.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
