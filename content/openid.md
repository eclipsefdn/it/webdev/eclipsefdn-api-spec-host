---
title: Eclipse Foundation OpenID
date: 2024-02-25T09:00:00-08:00
---

The Eclipse Foundation provides user authorization from third-part websites and tools using OpenID Connect.

To enable a site or application to leverage a users' Eclipse account, a request must be made to the webdev team. 

## Who can request an OpenID Client ID

1. Eclipse Projects who wish to use Eclipse accounts for Eclipse project-related content and applications;
2. Eclipse Committers who wish to operate services for the Eclipse Community, as approved by the Eclipse Management Organization;
3. Eclipse Members (Solutions Members and Strategic Members) who wish to operate services for the Eclipse Community, as approved by the Eclipse Management Organization;


## Revoking a Client ID

The Eclipse Management Organization (EMO) may, at its discretion, choose to revoke a Client ID at any time. The reasons may include:

1. Faulty software which generates too many requests to our service;
2. Abuse of any kind;
3. Usage for other purposes that are not Eclipse-related.


## Requesting a Client ID

To request a client_id and a client_secret, please open an issue in our helpdesk. Please specify the following:

1. Your Eclipse-related project or member affiliation;
2. The purpose of the request (why do you need Eclipse authorization?);
3. Where this authorization will be used (website, rich client app);
4. An estimate on how many user authentication/authorization calls per day we can expect to receive.
5. The callback URL of your application. This is the url where the user should be returned to after signing in.


## Endpoint (URL's)

Authorization endpoint: https://accounts.eclipse.org/oauth2/authorize

Token endpoint: https://accounts.eclipse.org/oauth2/token

User information endpoint: https://accounts.eclipse.org/oauth2/UserInfo


## Scope Values

OpenID Connect Clients use scope values to specify what access privileges are being requested for Access Tokens.

**openid** 

REQUIRED. Informs the Authorization Server that the Client is making an OpenID Connect request. If the openid scope value is not present, the behavior is entirely unspecified.

**profile**

OPTIONAL. This scope value requests access to the End-User's default profile Claims, which are: name, family_name, given_name, middle_name, nickname, preferred_username, profile, picture, website, gender, birthdate, zoneinfo, locale, and updated_at.

**email**

OPTIONAL. This scope value requests access to the email and email_verified Claims. 