---
title: "Eclipse Foundation API Specifications"
date: 2023-01-10T09:03:20-08:00
layout: single
---

See the [Eclipse Foundation OpenID page](openid) for more information on connecting to authenticated endpoints. 

{{< spec_list >}}